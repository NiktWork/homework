import os
import re
import argparse

class IgnoredFile:
    def __init__(self, path, rule):
        self.path = path
        self.rule = rule

def check_ignored(project_dir):
    parser = argparse.ArgumentParser()
    parser.add_argument('--project_dir', type=str, help='Path to the project directory')
    args = parser.parse_args()
    project_dir = args.project_dir

    gitignore_path = os.path.join(project_dir, '.gitignore')
    if not os.path.exists(gitignore_path):
        print('No .gitignore file found in the project directory')
        return

    with open(gitignore_path, 'r') as f:
        gitignore_rules = f.readlines()

    gitignore_rules = [rule.strip() for rule in gitignore_rules]
    gitignore_rules = [rule.replace("/", "\\") for rule in gitignore_rules if rule and not rule.startswith('#')]

    all_files = []
    for root, dirs, files in os.walk(project_dir):
        for file in files:
            all_files.append(os.path.join(root, file))

    ignored_files = []
    for rule in gitignore_rules:
        if rule.startswith('*'):
            for file in all_files:
                if file.endswith(rule[1:]):
                    ignored_files.append(IgnoredFile(file, rule))
        else:
            for file in all_files:
                if rule in file:
                    ignored_files.append(IgnoredFile(file, rule))          
    
    for file in ignored_files:
        file.path = file.path.replace(project_dir + os.sep, '') 
    ignored_files = list(set(ignored_files))

    if ignored_files:
        print('Ignored files:')
        for file in ignored_files:
            print(file.path.replace("\\", "/"), 'ignored by expression', file.rule.replace("\\", "/"))
    else:
        print('No ignored files found')

if __name__ == '__main__':
    check_ignored(None)
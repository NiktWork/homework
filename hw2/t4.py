def number_of_unique_characters(s : str):
    a = {}
    
    while len(s) != 0:
        c = s[0]
        a[c] = s.count(c)
        s = s.replace(c, '')

    return a

if __name__ == '__main__':
    print(number_of_unique_characters(input("Input: ")))
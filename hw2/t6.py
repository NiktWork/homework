def format_number(number):
    number = round(number, 3)
    number_str = str(number)[::-1]
    number_len = len(number_str)
    number_str = ' '.join([number_str[number_len-i*3:number_len-(i+1)*3] for i in range(number_len//3+1)])

    print(number_str)


format_number(123456789)

#number = float(input("Number: "))
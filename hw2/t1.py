def fib(n):
    if n == 0:
        return 0
    if n == 1:
        return 1
    
    return fib(n-1) + fib(n-2)

def fibc(n):
    prev = 0
    now = 1

    for i in range(0, n-1):
        now = prev + now
        prev = now-prev

    return now

if __name__ == '__main__':
    n = int(input("Input number: "))
    print("FIB: ", fib(n), fibc(n))
def two_sum(arr: list, num: int):
    for i in range(0, len(arr)):
        el = arr[i]
        if num != 2*el:
            k = 0
        else:
            k = 1
            
        if arr.count(num-el) > k:
            for j in range(0, len(arr)):
                if arr[j] == num-el and i != j:
                    return [i, j]
                
if __name__ == '__main__':
    print(two_sum([2, 6, 11, 15], 9))
def one_plus(arr):
    arr[-1] += 1

    for i in range(len(arr)-1, 0, -1):
        if arr[i] > 9:
            arr[i-1] += 1
        arr[i] %= 10

    if (arr[0] > 9):
        arr[0] %= 10
        arr.insert(0, 1)
    return arr

def plus_one(arr):
    return [int(i) for i in str(int(''.join([str(i) for i in arr]))+1)]

if __name__ == '__main__':
    arr = [9, 9, 9, 9, 9, 9]
    one_plus(arr)
    print(arr)

    arr = [9, 9, 9, 9, 9, 9]
    print(plus_one(arr))

    # 123
    # 1234